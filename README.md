### 项目介绍
    
　　jruc-admin为使用jruc([jruc项目地址](http://git.oschina.net/rlaxuc/jruc))开发的后台管理系统基础版，可用作平台系统脚手架来用，详细注释了jruc开发框架的使用说明，包含认证权限，系统日志，数据字典，简单CMS，OSS存储，微信，短信接口等功能，包含很多jfinal实战开发技巧，无论是学习jfinal还是作为脚手架开发都是一个不错的选择。   

[jruc项目地址](http://git.oschina.net/rlaxuc/jruc)

 **感兴趣有疑问的可以加群交流：285764382** 

### 技术选型

 - 核心框架：jfinal
 - 模版引擎：beetl
 - 安全框架：shiro
 - 数据连接池：druid
 - 调度框架：quartz
 - 缓存框架：ehcache/redis
 - 日志框架：slf4j
 - 爬虫框架：webmagic
 - 文档操作：easypoi
 - 富文本编辑器：ueditor
 - 单点登录：cas/kisso
 - 对象存储：阿里云oss/七牛云存储
 - 短信接口：阿里云短信
 - 微信框架：jfinal-weixin
 - jfinal扩展：jfinal-ext2
 - 页面ui：ace-admin/zui/weui

### 项目使用

　　请使用jdk7及以上版本，目前jruc还没有传到maven中央库，只能通过手动安装    
　　如果你已经编辑安装jruc到maven本地库，引入jruc-admin即可。   
　　如果你只引入了 jruc-admin 项目，可复制jruc-admin/doc/maven/com 文件夹到你的 mavne 本地库根目录，然后引入jruc-admin即可  
　　数据库脚本：jruc-admin/doc/mysql/jruc-admin.sql，建立数据库，在jruc-admin/src/main/resources/cfg.txt内配置数据库链接，数据库密码使用下面命令加密。   
```
使用命令生成数据库密码 `java -cp druid-1.0.5.jar com.alibaba.druid.filter.config.ConfigTools your_password`
```
　　执行jruc-admin/src/main/java/app/AppConfig.java启动
![输入图片说明](https://git.oschina.net/uploads/images/2017/0511/200647_17860036_7637.png "启动说明")
    
### 演示页面

登录界面：
![登录界面](https://git.oschina.net/uploads/images/2017/0511/201020_6940f39e_7637.png "登录界面")
资源管理：分为菜单与功能两种类型，菜单为左侧菜单树，功能为页面具体按钮或超链或其他资源内容，配置在资源管理内的资源只有通过授权后才可进行访问。
![资源管理](https://git.oschina.net/uploads/images/2017/0511/204440_aaf7dba1_7637.png "资源管理")
用户管理：配置系统的登录用户，并配置改用户角色。
![用户管理](https://git.oschina.net/uploads/images/2017/0511/201119_eed04143_7637.png "用户管理")
角色管理：配置系统内角色，并配置角色具有哪些资源的权限。
![角色管理](https://git.oschina.net/uploads/images/2017/0511/201154_db214fe9_7637.png "角色管理")
数据管理：配置系统所需的字典参数，存储在缓存内，可灵活使用。
![数据管理](https://git.oschina.net/uploads/images/2017/0511/201214_a3bab33e_7637.png "数据管理")
日志管理：记录系统每次访问者的请求信息
![日志管理](https://git.oschina.net/uploads/images/2017/0511/201236_cbc1deab_7637.png "日志管理")
数据库监控：druid数据源监控
![数据库监控](https://git.oschina.net/uploads/images/2017/0511/201302_2039bfcc_7637.png "数据库监控")
页面管理：简单的CMS功能，已做缓存处理，可供其他地方使用
![页面管理](https://git.oschina.net/uploads/images/2017/0511/201341_c4ff8e5c_7637.png "页面管理")


### 感谢

在项目搭建过程中，吸取了以下项目的很多代码启发，非常感谢！
- [jfinal](http://git.oschina.net/jfinal/jfinal)
- [JFinal_Authority](http://git.oschina.net/jayqqaa12/JFinal_Authority)
- [JFinal-ueditor](http://git.oschina.net/596392912/JFinal-ueditor)
- [jquery-weui](http://git.oschina.net/edik/jquery-weui)
- [JFinal-ext2](http://git.oschina.net/brucezcq/JFinal-ext2)